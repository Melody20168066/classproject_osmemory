package Adapter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

import Core.MemoryBlock;
import UesrInterface.UI;

public abstract class Adapter
{
    public LinkedList<MemoryBlock> table;
    public ArrayList<Integer> tags;
    UI ui;

    public Adapter(UI ui, int maxMemorySize)
    {
        this.ui=ui;
        table = new LinkedList<>();
        tags = new ArrayList<>();
        MemoryBlock blank = new MemoryBlock(0, maxMemorySize ,0);
        blank.setMemoryState(false);
        tags.add(0);
        table.add(blank);
    }
    public Adapter(UI ui)
    {
        this(ui,640);
    }

    public abstract void insert(int length, int tag);

    public void release(int tag)
    {
        // release memory, and merge empty memory blocks from left to right.
        try
        {
            if(tags.get(tag) == -1)
            {
                //System.out.println("Job "+tag+" has already been released.");
                ui.appendBoard("Job"+tag+" has been released.\n");
                return;
            }
            tags.set(tag, -1);
        }
        catch(RuntimeException err)
        {
            //System.out.println("No such memory block.\n");
            ui.appendBoard("No such memory block.\n");
        }

        ListIterator<MemoryBlock> tableIterator = table.listIterator();
        while(tableIterator.hasNext())
        {
            MemoryBlock nowBlk = tableIterator.next();
            if(nowBlk.getTag()==tag)
            {
                nowBlk.setMemoryState(false);
                //System.out.println("Job "+tag+" is released.");
                ui.appendBoard("Job "+tag+" is released.\n");
            }
            if(!nowBlk.getMemoryState() && tableIterator.hasNext())
            {
                MemoryBlock nxtBlk = tableIterator.next();
                if(!nxtBlk.getMemoryState())
                {
                    nowBlk.setEnd(nxtBlk.getEnd());
                    System.out.println(nxtBlk.getTag()+" Removed");
                    tableIterator.remove(); // FIXME: Am I doing right?
                }
            }
        }
    }
}
