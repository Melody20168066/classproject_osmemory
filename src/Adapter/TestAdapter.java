package Adapter;

public class TestAdapter
{
    public static void main(String args[])
    {
        FirstFitAdapter ffAdapter = new FirstFitAdapter(null);
        ffAdapter.insert(100,1);
        ffAdapter.insert(200,2);
        ffAdapter.insert(800,3);

        ffAdapter.release(1);
        ffAdapter.insert(80 ,4);
        ffAdapter.release(2);
        ffAdapter.release(2);
        ffAdapter.release(5);
    }
}
