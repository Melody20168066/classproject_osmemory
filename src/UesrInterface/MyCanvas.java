package UesrInterface;

import java.awt.*;
import java.util.Random;

// TODO: Work of Task 4
public class MyCanvas extends Canvas
{
    private Graphics pen;
    private Color color;
    private int wide;
    static private Random rand= new Random();
    private int start;
    private int length;

    MyCanvas(Color color, int start, int wide)
    {
        this.wide = (int) (wide * 1.56);
        length = wide;
        setSize(this.wide, 350);
        this.color = color;
        this.start = start;
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        pen = g;
        pen.setColor(color);
        pen.fillRect(0, 0, wide, 350);
        pen.setFont(new Font("Noto Sans SC",Font.BOLD,12));
        pen.setColor(Color.orange);
        int randHeight= rand.nextInt(290) + 30 ;
        pen.drawString(String.valueOf(start) + "K", 5, randHeight);
        pen.drawString(String.valueOf(start + length) + "K", wide - 40, 350-randHeight);
    }

    public Color getColor()
    {
        return color;
    }
}